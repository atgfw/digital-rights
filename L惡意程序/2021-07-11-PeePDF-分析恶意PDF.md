---
---


# 用 PeePDF 分析恶意的 PDF 文档

**作者：Lenny Zeltser**  
**译者：atgfw**  
原文：https://zeltser.com/peepdf-malicious-pdf-analysis/  

攻击者一直在使用恶意的 PDF 文件作为攻击的一个方法，特别是在执行大规模的客户端的攻击。 [Peepdf]()，是由 Jose Miguel Esparza 开发的一款很棒的用于检验和解码恶意 PDF 的分析工具。

在这篇介绍性的文章里我来用从 [Contagio Malware Dump](http://contagiodump.blogspot.com/2010/11/adobe-0-day-xplpdf-poc-from-scup.html) 获得的恶意 PDF 来做个快速的分析。

Peepdf 用 Python 写成，并没有图形界面，习惯于命令行的用户应该会觉得还是比较舒服的，尤其是它的交互模式啦，可以提供一个 shell 让你轻松地在 PDF 文件的结构里遨游，探索文件的内容。  


## 查验 PDF 有无可疑的特征

安装 Peepdf 之后（安装方法在后文），你可以用 `peepdf file.pdf` 轻松地扫一扫这个 PDF 文件的基本信息。当你用交互模式时，可以用 `info` 命令查看。  

用 `info` 命令时，Peepdf 就会把攻击者常用于攻击的可疑对象列了出来。下面这个例子，就是 Peepdf 高亮了 AcroForm, OpenAction, Names, JS and JavaScript，因为这些 PDF 元素常常被滥用了。还指出的是 `Object 13` 看上去包含了 Javascript 代码，这是 PDF 攻击中的常见组成成分。  

![](https://cdn.zeltser.com/media/archive/peepdf-info-full.png)

用 `metadata` 命令（在交互模式），也能看到 PDF 文件里的元数据：  
```
  PPDF> metadata
  
  Info Object in version 0:
  
  /Title
  /ModDate 2008312053854
  /CreationDate 2008312053854
  /Producer Scribus PDF Library 1.3.3.12
  /Trapped /False
  /Creator Scribus 1.3.3.12
  /Keywords
  /Author
```


## 查验 PDF 中可疑对象里的内容

使用 `-i` 参数进入 Peepdf 的交互模式：  
```
  peepdf -i file.pdf
```
然后可以使用 `object` 命令查看某个对象（object）的内容。比如，我们刚才发现 object 13 里有 Javascript 代码，输入 `object 13` 就会显示这个对象的内容，包括内嵌的 javascript 代码。Peepdf 会自动解码包含 js 代码的内容。  

![](https://cdn.zeltser.com/media/archive/peepdf-object-13-full.png)

我们这个例子里，变量 `large_hahacode` 看着会包含 shell 代码，我们想解开并分析分析它有多大威力。  

Peepdf 还带有更多的命令来帮你分析内嵌的 Javascript 变量和命令。Peepdf 还提供了一个方便的调用 `[sctest](http://code.google.com/p/peepdf/wiki/Commands#sctest)` 命令的方法，能帮你模拟地执行 shell 脚本。  

想学习更多 Peepdf 的分析恶意 PDF 的功能的话，可以看看它的官网：[usage](http://eternal-todo.com/tools/peepdf#usage)，和[所有命令列表](http://code.google.com/p/peepdf/wiki/Commands)。


## 安装 Peepdf on Linux

（译注：作者写此文时的 Peepdf 是0.1。依据最新的 Github repo，最新的安装方法非常简单，从 Github 下载代码后运行即可）

```
  (下载并解压) https://github.com/jesparza/peepdf/archive/refs/heads/master.zip
  cd peepdf-master
  python2 peepdf.py -h
```
（基本功能不需要依赖；如需要解码/解压缩图像，需安装 Pillow (brew install pillow)；其他可选依赖请参考 [wiki](https://github.com/jesparza/peepdf/wiki/Installation)）  



