---
date: 2021-10-30
---



# BleachBit: 关于擦除文件的迷思  

*译者：atgfw*   
原文： https://docs.bleachbit.org/doc/shred-files-and-wipe-disks.html  

> 缘起：这篇来自开源的文件清理软件 BleachBit 的文档，对于「彻底擦除」这件事有独到的理解，值得有需要的人士认真理解和思考。（本文是对原文的非完全编译）  

> **任何宣称能对数字安全方面有既方便又有效的解决方案，都是骗子**。安全和便利永远两难全。  

> 对于数据安全删除，**「一遍总是足够，35遍总是不够」**。  


## 都市传说

众所周知，现代的计算设备上删除磁盘数据的方法，只是简单的把元数据（metadata）删去，数据的内容还遗留在磁盘上，还有可以被恢复的可能性。因此无论专业领域还是坊间，都流传着关于如何将数据彻底擦除的「都市传说」。比如有 Gutmann 的35次覆盖法、美国国防部7次标准、NSA的标准等等。  

为了破除这些迷思，先来看看哪些观点是 false：  
- False：硬盘上的数据被覆盖一次后还是可以被强大的政府机构恢复。  
- False：多次覆盖数据可以比覆盖一次更难恢复。  
- False：Peter Gutmann 认为为防恢复，数据应被覆盖35次。  
- False：Gutmann 的论文也适用于现代硬盘。  
- False：美国防部 (5220.22-M)、NSA 和 Gutmann 都官方认证了一些擦除文件的方法。  
- False：美国防部认定了覆盖整个硬盘作为数据清理的方法。 


## BleachBit 有哪些安全擦除功能

（简单来说）
- 将数据所在磁盘的位置覆盖空数据一次（需开启 overwrite 功能）；
- 在被删除的文件末尾，写入多余的数据来抹掉空闲的空间（slack space）； 
- 把原文件名改名，先改成一个长的随机名字，再改成一个短的名字。  
- (on Linux) 擦除内存（RAM）和 Swap 空间里的文件数据（如有）。  


## 这些功能安全么？

这取决于你怎么定义安全。  
当你问一个锁匠，我是否该给我们的前门升级一下门锁呢？这个锁匠（很明显不会做生意）回：“干嘛这么破费？盗贼还是会破窗而入呢。”  

驾驶一辆配备了安全气囊（并经过碰撞测试）的车安全吗？可能，但难说，因为这要看是什么人开车。一个15岁的毛小子在半夜的高速路上开，还边开边看手机？那就不安全了。  

这个比喻要说明：一、安全不是非黑即白；二、场景的评估很重要，一个方案、技术在某个场景有效，不一定在另个场景也一定有效。 

## 提出正确的问题  

比获得正确答案更重要的，是开始对自己提出正确的问题：  
- 我有哪些东西需要保护（包括删除）？ 
- 如果保护失败，会带来多大影响/伤害？ e.g. 影响自己前程、伤害/骚然他人、破坏了人际关系、甚至牢狱之灾……  
- 你的对手是谁？哪些人会对你的数据感兴趣？  
- 对手会有多大时间、金钱、精力、技能来取得你的数据？  

以上4个问题请记住，你便清晰地知道你会采取哪些措施。  


## 多次擦除会比单次擦除好？  

一些相关软件会提供「高级」和「专业」的多次擦除功能，如 Gutmann 35 次覆盖法、国防部 7次标准、国安局 3次标准等等。然而这是一种曲解。 Peter Gutmann 后来对自己早前的论文的观点进行了[澄清](https://www.cs.auckland.ac.nz/~pgut001/pubs/secure_del.html#Epilogue)：  

> Some people have treated the 35-pass overwrite technique described in it more as a kind of voodoo incantation to banish evil spirit. [… ] In fact performing the full 35-pass overwrite is pointless for any drive since it targets a blend of scenarios involving all types of (normally-used) encoding technology.

维基百科的[解释](https://secure.wikimedia.org/wikipedia/en/wiki/Data_remanence#Feasibility_of_recovering_overwritten_data)也支持单次擦除是足够的：

> The chances of overwritten data being recovered from a modern hard drive amount to “urban legend”. He also points to the “18 1/2 minute gap” Rose Mary Woods created on a tape of Richard Nixon discussing the Watergate break-in. Erased information in the gap has not been recovered, and Feenberg claims doing so would be an easy task compared to recovery of a modern high density digital signal.

迄今也还没有一个计算机科学家的研究有指出单次擦除是一定可以恢复的。  

此外， 各种擦除软件通常过分粉饰了它们的这些技术。然而国防部 5220.22-M 标准其实从不是针对擦除单个文件或抹掉空余空间的，它用于指导如何清空磁盘所有数据以便报废掉磁盘。而且它还规定了它所认准的方法只适用于国防部内部，任何要流出国防部的存储设备，都只有一个认准的方法：物理毁坏。换言之，任何所谓安全擦除软件其实都未得到国防部或国安局的认证。  


## 擦除文件和空闲空间的局限  

擦除文件 (shred) 和空闲空间 (wipe free disk space)的方法本身是有局限的。当你明白了其中的原理后，你就知道你还需采取什么行动了。  

在理想情况下，擦除一个文件（不管是 BleachBit 还是其他软件）都需要知道该文件在磁盘的全部位置。但实际上这种理想情况很少出现，因为其需满足这几个条件：  

- 该文件的大小不变，特别是没缩小过。有时我们编辑一个文件，编辑后尺寸是会变小的（比如从 3MB 变 2MB）。这时擦除软件是无从知道这 1MB 是在什么地方（通常也不能假定那部分数据和现有数据是连在一起的）。 看图，红色方块表示被缩减后属于原文件的数据。  
![](https://lh5.ggpht.com/_1XYQfEGGEIw/TMG_UyXYeVI/AAAAAAAACx4/mclA1xjpxlE/s800/file_shred_graphics.png)  
- 文件是从未移动过的。同理，擦除软件也无法知道该文件之前去过磁盘的什么位置。现代操作系统和软件通常是这样移动文件的：把原文件复制到一个临时文件，删掉原文件，再把临时文件的文件名改回原文件的名称。  
- 磁盘上的文件系统（filesystem）对于覆盖操作必须忠实，即去覆盖该文件所在的位置。一些「高级」的磁盘系统，如透明磁盘压缩、加密，不会对文件就地覆盖（也就是没覆盖到）。幸运的是，Windows 的 NTFS，大部分 Linux 默认的文件系统 ext3/4 都是原地覆盖的操作。  
如果 BleachBit 无法知道文件在删除或移动之后的位置，那其他任何软件也不会知道。其实当文件被擦除一次后，即使有数据碎片散落在茫茫磁盘的海洋里，要恢复回一个完整可读的文件，几乎是不可能的。  

> 译者注：这里要提醒 Linux / macOS 用户，不要使用带 journal 或 snapshot 功能的文件系统，如 ZFS、btrfs、启用了 journal 的 ext4、APFS 等等，「日志」或「快照」会记录过往的数据状态。  

那么，对于常常出现的不能满足三个条件的非理想状况，扫除空余空间（wipe）可以解决一定程度的问题。 然而，它也会遇到一些挑战：  

- 会超慢，很多人都等不了（具体时长取决你的磁盘大小和电脑处理速度，对于 200GB+ 通常都需一整夜；除非你的磁盘非常小，比如 几GB～几十GB，因此建议平时就使用U盘存储 —— 译者注）  
- 文件系统对磁盘的操作方式是以一块一块（block）的方式进行的，每次操作一个 block 的单位（chunk）。虽然可以对 block 的大小进行设定，但通常都无法被整个磁盘的 size 整除，也就是还留下没有操作的一点点空间（slack space）。如果数据写到这个空间里，任何软件都是操作不到这里的。  
- 现在磁盘中，如果有的区域是损坏的，会自动被标记（remap）成坏块（bad sector），之后的操作都不会来动它，并且那上面的数据会被复制到一个好的块里。那么操作系统和软件其实感知不到这种操作的，因此擦除软件也无法擦除这部分的数据的。  


## 那我该怎么办？  

这里有个清单，从**最方便/最不安全 -- 最安全/最耗时**的顺序列出，最方便/快速的（第一条）也通常是最不安全的，切记： 

- 擦除（shred）一次该文件（是指用 BleachBit 啦，不是清空回收站）；
- 覆盖空余磁盘空间；  
- 覆盖整个磁盘（不只是分区），包括系统文件都没有了；  
- 物理毁坏/消磁磁盘；
- 如果这些数据还存在别的什么地方，记得清理，包括网盘、邮箱、社交账号等等。  
- 译者注：不要问云端存储安不安全啦，首先文件上去了，就无法像在本地那么多方法擦除了，你也无法信任网页上的「删除」按钮；再者如果你的数据非常敏感、你的对手是政府，你可能需要担心你的网盘服务商会不会屈从于政府，你可能还要担心你会不会被逼问网盘的密码……  

## 总结

**对于大多数人的大部分数据来说，「一遍总是足够的」，对于特别敏感的数据，「35遍总是不够的」，或许需要的是物理毁坏**。  

祝好，祝安全～！

