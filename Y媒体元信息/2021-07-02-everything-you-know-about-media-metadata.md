---
---



# 關於多媒體文件元數據，你需要知道這些  

*作者：Harlo Holmes*  
*譯者：Anon*  
原文：https://freedom.press/training/everything-you-wanted-know-about-media-metadata-were-afraid-ask/

> 元數據，有時候會給我們帶來很多便利，比如你可以通過照片的拍攝日期或地點來歸類照片。但如果落入壞人手裡，同樣的信息就會帶來損害。  


## 什麼是元數據（metadata）

元數據就是關於數據的數據。在數字世界每個東西都有元數據，裡面描述了關於任何文檔、視頻、圖片或音頻的 “誰、什麼、何時、何處、如何”以及甚至還有“為何”等的信息。元數據，有時候會給我們帶來很多便利，比如你可以通過通過照片的拍攝日期或地點來歸類照片。但如果落入壞人手裡，同樣的信息就會帶來損害。  


## 所以，元數據長啥樣？

元數據就存在圖片、視頻或音樂文件裡，我們人類是覺察不到的。但如果你有專門的工具來查看這些文件，你就能看到大量的屬性信息（或標簽）。其中一個最簡單的標簽就是“創建日期”，紀錄了拍攝者按下快門或攝錄按鈕的那個時刻。其他有趣的標簽包括“Make” 和 “模式”，可以告訴你生成這個媒體文件的相機或電腦類型。還有其他數十個這樣的標簽，這也就是為什麼元數據能提供豐富的信息來標識出創作或分享這個媒體文件的人。  


## 設置一個媒體工作站“沙盒”  

這篇指引文章裡提到的大部分工具都可以在大多數操作系統上運行。要記住當你在你的電腦上用這些工具把元數據輾壓得粉碎的同時，也要把你自己的隱私和安全保護好。你可能要對非常敏感的材料進行操作，所以建議還是不要在你的日常的電腦進行這些操作。  

為了安全的操作敏感文件，我發現最安全且方便的方法就是在一個隔離的、劃分了單獨的空間的環境裡，進行諸如剪裁、切、複製和粘貼等操作。我給我自己構建的環境（即操作系統）叫“沙盒”（sandbox）：一個安全的操作危險東西的地方。

## Tails

（译略，Tails 是一种 Live 型操作系统，将另行介绍 Tails）

**譯者注：除了 Tails live 系統，開源軟件社區還為所有人提供更多的選擇，如：[antiS](https://github.com/mdrights/liveslak), [Remnux](https://remnux.org), [Kali](https://kali.org), [AntiX](https://antixlinux.com)等等。**  

## 使用元數據工具探索元數據

### 用 Exiftool 分析元數據

[Exiftool](https://exiftool.org/) 是個可以讓你分析、編輯和清理元數據的開源軟件。雖然它能處理很多文件類型（圖像、視頻、音頻、文本等等），但它無法刪除或覆蓋所有的元數據（除了幾種簡單的圖片格式）。 還有更好的工具和流程來把元數據完全刪除掉，我們會在後面章節介紹。  

在本章，我們來用 Exiftool 深入探索元數據。  

**注**：请使用 Exiftool 12.24 或以上，其修复了一个安全漏洞。  

### 例子：來自 Flickr（.jpg）的圖片

通過這個命令，我們就能看到這個圖片的所有歷史。  
```
me@computer:~$ exiftool idied.jpg
ExifTool Version Number         : 10.71
File Name                       : idied.jpg
Directory                       : .
File Size                       : 170 kB
File Modification Date/Time     : 2018:01:04 01:06:30-05:00
File Access Date/Time           : 2018:01:04 01:06:31-05:00
File Inode Change Date/Time     : 2018:01:04 01:06:31-05:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Exif Byte Order                 : Little-endian (Intel, II)
Make                            : EASTMAN KODAK COMPANY
Camera Model Name               : KODAK EASYSHARE C653 ZOOM DIGITAL CAMERA
Orientation                     : Rotate 270 CW
X Resolution                    : 480
Y Resolution                    : 480
Resolution Unit                 : inches
Y Cb Cr Positioning             : Co-sited
Exposure Time                   : 1/13
F Number                        : 4.6
Exposure Program                : Program AE
ISO                             : 160
Exif Version                    : 0221
Date/Time Original              : 2006:01:09 07:25:05
Create Date                     : 2006:01:09 07:25:05
Components Configuration        : Y, Cb, Cr, -
Shutter Speed Value             : 1/13
Aperture Value                  : 4.8
Exposure Compensation           : 0
Max Aperture Value              : 4.8
Metering Mode                   : Multi-segment
Light Source                    : Unknown
Flash                           : Off, Did not fire
Focal Length                    : 18.0 mm
Serial Number                   : KCFGP71706722
Flashpix Version                : 0100
Color Space                     : sRGB
Exif Image Width                : 2848
Exif Image Height               : 2144
Interoperability Index          : R98 - DCF basic file (sRGB)
Interoperability Version        : 0100
Exposure Index                  : 160
Sensing Method                  : One-chip color area
File Source                     : Digital Camera
Scene Type                      : Directly photographed
Custom Rendered                 : Normal
Exposure Mode                   : Auto
White Balance                   : Auto
Digital Zoom Ratio              : 0
Focal Length In 35mm Format     : 108 mm
Scene Capture Type              : Standard
Gain Control                    : Low gain up
Contrast                        : Normal
Saturation                      : Normal
Sharpness                       : Normal
Subject Distance Range          : Unknown
Compression                     : JPEG (old-style)
Thumbnail Offset                : 12214
Thumbnail Length                : 5778
Image Width                     : 1280
Image Height                    : 963
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Aperture                        : 4.6
Image Size                      : 1280x963
Megapixels                      : 1.2
Scale Factor To 35 mm Equivalent: 6.0
Shutter Speed                   : 1/13
Thumbnail Image                 : (Binary data 5778 bytes, use -b option to extract)
Circle Of Confusion             : 0.005 mm
Field Of View                   : 18.9 deg
Focal Length                    : 18.0 mm (35 mm equivalent: 108.0 mm)
Hyperfocal Distance             : 14.07 m
Light Value                     : 7.4
```

看，一張照片就有這麼信息啦！我們可以知道，2006年的某天，有個人用我的 Kodak EasyShare 相機給我拍了照。光源、缺乏閃光和光圈是拍攝者設置的。最重要的，你也可能注意到了，是“Serial Number” 標簽 —— 現在大家都知道了我在 00 年代早期擁有了這部相機。  

幸運的是，我們可以用 Exiftool 把這個照片的所有個人信息都抹掉。  

```
me@computer:~$ exiftool “-all=” idied.jpg
```

這條命令對 .jpg 格式圖像都很好使，但並不保證對其他格式文件都好使（所以請接著看下文）。  

**Exiftool 在 macOS 的安装**  
```
  % brew install exiftool
```


### 例子：一個來自互聯網檔案館（The Internet Archive）的很酷的博客（.mp3）  
```
me@computer:~$ exiftool RubenerdShow363.mp3
ExifTool Version Number         : 10.71
File Name                       : RubenerdShow363.mp3
Directory                       : .
File Size                       : 23 MB
File Modification Date/Time     : 2018:01:03 14:11:11-05:00
File Access Date/Time           : 2018:01:03 21:29:45-05:00
File Inode Change Date/Time     : 2018:01:03 14:11:14-05:00
File Permissions                : rw-r--r--
File Type                       : MP3
File Type Extension             : mp3
MIME Type                       : audio/mpeg
MPEG Audio Version              : 1
Audio Layer                     : 3
Audio Bitrate                   : 128 kbps
Sample Rate                     : 44100
Channel Mode                    : Joint Stereo
MS Stereo                       : On
Intensity Stereo                : Off
Copyright Flag                  : False
Original Media                  : True
Emphasis                        : None
Encoder                         : LAME3.99r
Lame VBR Quality                : 4
Lame Quality                    : 0
Lame Method                     : CBR
Lame Low Pass Filter            : 17 kHz
Lame Bitrate                    : 128 kbps
Lame Stereo Mode                : Joint Stereo
ID3 Size                        : 57034
Release Time                    : 2017
Original Release Time           : 2017:07:14
Recording Time                  : 2017:07:14
Encoding Time                   : 2017:07:14
Tagging Time                    : 2017:07:14
Picture MIME Type               : image/png
Picture Type                    : Front Cover
Picture Description             :
Picture                         : (Binary data 54706 bytes, use -b option to extract)
Lyrics                          : (SHOWNOTES) 25:22 Join Ruben as he harkens back to one of the first reboot episodes in 2015, when he was also wandering around an empty house that was once his home. Two years later, and he's moving out of the place he moved away from that earlier place to. This show description had several variants of the word move in it. Recorded 3rd of July 2017...Recorded in Sydney, Australia. Licence for this track: Creative Commons Attribution 3.0. Attribution: Ruben Schade...Released July 2017 on Rubnerd and The Overnightscape Underground, an Internet talk radio channel focusing on a freeform monologue style, with diverse and fascinating hosts...
Track                           : 363
Artist                          : Ruben Schade
Album                           : Rubnerd Show
Band                            : Ruben Schade
Title                           : 363: The everything except episode
Genre                           : New Time Radio
Publisher                       : Ruben Schade
Internet Radio Station Name     : Overnightscape Underground
Internet Radio Station Owner    : Frank Edward Nora
File URL                        : https://archive[.]org/download/RubenerdShow363/RubenerdShow363.mp3
Artist URL                      : https://rubenerd[.]com/
Source URL                      : https://rubenerd[.]com/show363/
Internet Radio Station URL      : https://onsug[.]com/
Copyright URL                   : http://creativecommons[.]org/licenses/by/3.0/
Publisher URL                   : https://rubenerd[.]com/show/
Date/Time Original              : 2017:07:14
Duration                        : 0:25:18 (approx)
```
 
### 例子：一个从办公室扫描仪来的 PDF（.pdf）
```
me@computer:~$ exiftool Anonymous\ Witness\ 1\,\ Union\ Laborer_3.13.90.pdf
ExifTool Version Number         : 10.71
File Name                       : Anonymous Witness 1, Union Laborer_3.13.90.pdf
Directory                       : .
File Size                       : 1849 kB
File Modification Date/Time     : 2017:12:15 04:53:38-05:00
File Access Date/Time           : 2017:12:15 04:53:38-05:00
File Inode Change Date/Time     : 2018:01:04 01:22:47-05:00
File Permissions                : rw-r--r--
File Type                       : PDF
File Type Extension             : pdf
MIME Type                       : application/pdf
PDF Version                     : 1.4
Linearized                      : No
Creator                         : KMBT_283
Producer                        : KONICA MINOLTA bizhub 283
Create Date                     : 2017:02:14 17:58:02-05:00
Page Count                      : 8
```

你注意到 “Creator”和“Producer”标签了么？这些信息或许就能指出这份文档是从哪栋大楼、哪个办公室的扫描仪上扫描出来的。  


### 所以你现在知道了吧

再说一说，Exiftool 是个用于确保无误的检查工具（as a sanity check）。我们最好在每次用别的什么工具把元数据删除之后，拿这个工具做最终验证确认。好，那我们怎么能安全地删除元数据呢？  


## 管理媒体文件元数据

### 使用 MAT

如果你是 Linux 用户，[Metadata Anonymisation Toolkit](https://mat.boum.org/)(MAT)是一款非常强大的删除元信息的工具（译者注：想尝新的用户开始使用更新一代的 [MAT2](https://0xacab.org/jvoisin/mat2)）。它可以很好工作在诸如 .jpg, .mp3, .flac 和其他很多常用媒体类型上。  

![](https://media.freedom.press/media/images/mat2_context_menu.width-500.png)

（原文的使用方法是在 Tails 进行的，这里略去）

当然你仍然可以在任何 Linux 机器上命令行快速地使用它：  
```
  % mat2 /path/to/dirty/image.jpg
  （加 -s 参数可以只查看元数据而不删除）  
  % mat2 -s /path/to/dirty/image.jpg
```

**在 macOS 安装 MAT2**
```
  % brew install exiftool cairo pygobject3 poppler gdk-pixbuf librsvg ffmpeg mat2   
```

### 使用 FFmpeg  

[FFmpeg](https://www.ffmpeg.org/) 是一款很有爱的“瑞士军刀式”的万用音视频编辑工具，可操作的媒体类型很多，如 .mp4, .mov, .mkv, and .wmv。

```
ffmpeg -i /path/to/original/file.mp4 -map_metadata -1 -c:v copy -c:a copy /path/to/clean/clone.mp4
```

### 关于 Word、PDF 等的坏消息

上述的几个工具对于音视频文件都处理得非常好，但对于文字文档的处理则要复杂得多。像 .docx, .xlsx, .pdf, .ppt 和其他格式通常都会带多个内嵌的图片、视频和其他媒体文件。他们就像俄罗斯套娃。所以当要删除这些文档的元信息时，很容易忘了还要去删除这些文档里面的媒体的元数据。  

这时就有了另一个工具了：[Peepdf](https://github.com/jesparza/peepdf)，我们可以用它来查看 .pdf 文档里夹带了什么圖像，可能還有各种奇怪的东西（有时那些恶意的 PDF 文档就是夹带了恶意代码在里面！）（而且，我是不是忘了提醒你们，有时那些内嵌的图像可以做得非常小，甚至肉眼看不见的？）

因此，最好的防治这个问题的办法就是把所有内嵌的东西展开来（所谓“压扁”，flattening）。 对这些文档来说，就是要么把他们打印出来，再扫描进电脑；或者直接把文档导出（export）成另一个格式，再分享给别人。  

First Look Media 的 [PDF Redact Tools](https://github.com/firstlookmedia/pdf-redact-tools) （注：它已不再维护，请转为使用它的继任：[dangerzone](https://dangerzone.rocks/)）。它的功能是把 PDF 的每一页转为图片格式，再把它们重新拼接成一份新的 PDF 文档。但它有两个缺点：一是它转换完格式后会比较大；二是它依赖了另一个开源软件 ImageMagick —— 有时会有一些 bug —— 所以建议在一个隔离的、沙盒式的系统里运行。（注：第一个缺点已经在 Dangerzone 解决了）  

如果你想对这种“被压扁的” 文档做些基于文本的操作，比如 搜索关键词、字数统计，那么这种基于图像格式的文档就无法胜任了。还好，我们还有一款开源软件，叫 [Tesseract](https://github.com/tesseract-ocr/tesseract)，即 OCR 技术：图像转文字。所以你应该知道了怎么组合操作了。不过需要留意，现在的 OCR 技术还不完美，转成的文字还可能有瑕疵，尤其在转成非英语的语言的时候。

### 其他的修改工具

如果你不使用 Photoshop，可以用开源的 [GIMP](https://gimp.org/)，可以把图像转成 PDF 和其他文档格式。  

[Audacity](https://www.audacityteam.org/)，也是个开源的音频编辑工具。我发现它是个完美的访谈录音剪辑工具，可以剪去不宜发表的陈述。  

需要注意的是这种类型的编辑都是非破坏性的，即元数据、项目历史和原始文件可以通过专业取证软件还原回来。使用 GIMP 和 Audacity 时要小心地再去“压扁”（比如用下面介绍的“同源洞”方法）你的文件，然后用 Exiftool 检验有没有漏掉什么重要信息没删，最后再分享出去。  


## 同源洞（The Analog Hole）

虽然现在我们已经有很多很棒的工具来帮助我们理解、操作、和抹去元数据了，但是，道高一尺魔高一丈，专业的数字取证专家仍然有可能从取证到的数字东西（文件）中发现一些蛛丝马迹。那么一个有创造性的应对方式就是用所谓“同源洞”的方式来再造那些原来的文件/数据。  

你可曾经买过所谓的“枪版”（bootleg）电影？（可还行，我们不评价这个！） 如果你有，那你就明白了什么是“同源洞”了。所以你可以用类似方法来再造其他不同类型的文件（比较容易吧，不需要 Tails 等什么 live 系统）。  

### 一些“同源洞”主意  

- 图像：对着图像截图或拍照  
- 视频：用屏幕录制软件；有 macOS？用 QuickTime 呀。  
- 音频：用录音笔翻录（或可以用音频线可以直接把电脑声音传到录音笔）  
- docs/PDF：把文字内容拷贝到另一个新空白文档里；打印出来再扫描/拍照成数字版。  

### 还是会有坑

再说一遍，世上没有完美的东西。甚至各种“同源洞”都可能带来麻烦。举个例子，情报工作者们之间流传甚广的一招：把一份文档复制出很多份，基本一模一样但是！每个都包含着不同的小错误（如错别字）。那么，如果这份敏感文档被泄密者公开出来了，那么也就很容易定位和追溯到这位泄密者的身份了。 这就是无数的让信源暴露身份的招数中的其中一招，尽管你已经非常小心谨慎地来保护你的信源了。在接受信源提交的东西时一定要留意这些地方！  

祝使用順利～
